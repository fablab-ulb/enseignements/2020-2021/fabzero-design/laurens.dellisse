# Objet final 

![](images/gif_depliage.mp4)

## Principe 

La chaise LCP a été une source d’inspiration sous l’œil de l’extension. «L’extension du système constructif» de mise en tension d’une matière. Le travail et la recherche effectuée pour le projet store à donc essentiellement eu comme fil conducteur, la recherche structurelle.

Le début de la recherche propre à l’objet tendait à créer une chaise basse pliable et rangeable. Il s’agissait de pouvoir fabriquer une assise par la répétition d’élément simple de la même matière comme Maarten Van Severen l’a fait dans son travail pour la LCP et quelques années avant pour la LC95 ; une seule et même feuille de matière qui se déplie pour former une assise simple et épurée.
Il était donc impératif de trouver un système structurel permettant de passer d’un état structurel rigide lors de l’usage de l’objet à un état flexible rendant l’objet rangeable et mobile. C’est cette question qui forme la question principale du travail de recherche.
Le système structurel exploité est la mise en tension éphémère. Cette technique permet de passer d’un objet structurel à un objet détendu. Pour ce faire, plusieurs essais ont été réalisé sur une première forme d’assise concue en éléments en plastique recyclable moulé pour ensuite faire un pas en arrière et revenir sur une forme plus simple (tabouret) utilisant la répétition d’élément simple mit en tension par une sangle.
La recherche s’est donc appuyée sur différentes contraintes ; matière recyclable, éphémérité et système structurel alternatif.

![](images/objet_final_.jpg)

## Fabrication 
### Modelisation digitale 

La réalisation des différentes pièces, modules, formant l'assise est fabriquée par moulage. En effet, la matière plastique recyclée est fondue pour ensuite être pressée dans des moules en bois réalisés grace à l'usage d'une fraiseuse numérique. Pour ce faire, Les différentes pièces formant les moules doivent être modélisées puis simulées par l'option d'usinage du programme Autodesk Fusion 360. La simulation de découpe est propre à chaque machine et à chacun post-processeur, cependant, vous trouverez les modèles 3Ds des pièces nécessaires dans le lien Drive suivant : 

https://drive.google.com/drive/folders/1GhzoLXZNGCfqjLnGb65f6_lCYGpEOYCg?usp=sharing

![](images/modèle_3D.jpg)

### Découpe 

Comme dit plus haut, la découpe des moules est réalisée à l'aide de la CNC. Les pièces nécessaires sont découpées dans deux plaques de MDF. Un de 18 mm et une seconde de 12 mm. Le parcours complet de la fraiseuse prend au total 2h30 pour réaliser toutes les pièces nécessaires. 

![](images/IMG_1738.mov)

### Assemblage des moules

Une fois toutes les pièces découpées, celles-ci sont poncées puis collées en elle-même à l'aide de colle à bois. Pour assurer un bon maintien des déférentes pièces, des vis sont également ajoutées.

![](images/IMG_2052.JPG)

Après assemblage, les moules sont vernis en 3 couches sur les surfaces restées intactes et en 5 couches sur les surfaces coupées dans la tranche car la matière est beaucoup plus poreuse et absorbante. Il en résulte des moules beaucoup plus solides et moins adhésifs. Le choix de vernir les moules manne de différents échecs précédents. En effet, le MDF a tendance à se fissurer voir s'arracher lorsque l'on démoule les pièces en plastique. De potentiels futurs prototypent de moules devraient faire usage d'une autre matière fonctionnant aussi bien en compression (lorsque l'on presse la matière) qu'en traction (lorsque l'on démoule les pièces).

![](images/IMG_2053.JPG)

### Moulage du plastique

Le moulage du plastique doit d'abord commencer par récupération de celui-ci. Deux provenances de plastique ont été utilisées pour la fabrication des pièces. Une première source de plastique fut une ferme de la région de Louvain-la-Neuve. Celle-ci amassait du plastique d'emballage de ballot de paille et de sac de fourrage depuis plusieurs années. La seconde source de plastique fut "RECY-K". Il s'agit d'une start-up bruxelloise travaillant sur le recyclage du plastique en le transformant en plaque de construction. Ils ont gentiment accepté de me donner quelques kilos de plastique en copeau. 

![](images/HDPE_recy_K.jpg)

Le plastique récupéré est du HDPE. Ce plastique fond et devient malléable aux alentours de 170°C. On peut donc le faire fondre soi-même à l'aide d'un four ou d'un grill (dans mon cas). 

![](images/fondage.jpg)

Le plastique est roulé en boudin une fois ramolli. Les tous sont ensuite rechauffées quelques minutes. Cette opération est répétée plusieurs fois afin d'obtenir un boudin de la taille du moule.

![](images/boudin.jpg)

Le plastique est ensuite inséré dans le moule et pressé à l'aide de serre-joints ou d'une presse. Un capuchon pour chaque moule a également été découpé. Il permet de resserrer la matière et d'obtenir une pièce plus régulière.

![](images/moulagesss.jpg)

Après refroidissement des pièces, celles-ci peuvent être facilement démoulées puis nettoyée en enlevant l'excès de matière ayant débordé du moule lors du pressage.

![](images/IMG_2084_copie.jpg)

Il est important de préciser que l'opération doit être réalisée 21 fois pour les longs profilés. Ceux-ci nécessitent 300g de plastique chacun. Il faut également 18 pièces en biais nécessitant 50g de plastique et 34 pièces rectangulaires.

### Assemblage

De nombreux systèmes d'assemblages ont été testé (câbles, sangle, câble gainé, corde, ...). Le plus efficace reste l'utilisation de sangle munie de crochets car cela permet une mise en tension plus efficace de la structure. Les différents profilés en plastique sont collé et vissé à la sangle. Ils sont positionnés de sorte à laisser le moins d'espace entre eux afin que la tension soit la plus efficace.

![](images/assemblage.jpg)

Il est important de suivre l'ordre d'alternance des pièces de l'illustration suivante pour obtenir une bonne tenue de la structure.

![](images/IMG_2117.JPG)

### Mise en tension 

La mise en tension se fait grace à l'ajout de crochets et de boucles aux extrémités des sangles liant la structure.Une fois le voile de matière positionné, les attaches permettent de tenir toute la structure en tension. Cela permet de précontraindre l'objet dans sa totalité.

![](images/tension.jpg)

### Conslusion 

De manière générale, la recherche d'une structure pouvant se déplier et se plier en se rigidifiant fut plus complexe que prévu. Cela m'a amené à une simplification de la morphologie de l'objet en laissant les contre-courbes de côté comme initialement prévue.Il en résulte cependant, un objet relativement structurel et rangeable. 

Cependant, différents points peuvent être améliorés. Tels que le poids de l'objet et sa précontrainte afin d'obtenir une structure encore plus résistante.

# Recherches intermédiaires au projet final

## Prototypes morphologiques

Après avoir travaillé sur deux prototype montrant l'efficacité d'une structure mise en tension à l'échelle 1/4, j'ai continué ma recherche en amenant de nouvelles contraintes tel que l'économie de matière. J'ai donc réalisé un nouvel essai de forme en alternant des vides et des pleins afin d'alleger la structure. Pour ce faire, j'ai pris parti de garder une pièce unique et régulière pour les profilé traversant formant l'assise et de jouer sur des inclinaison de parois pour les profilés intermédiaires non-traversant. De cette manière, seuls les blocs de petite taille avaient la contraite d'un moule plus complexe intégrand des parois en biais.

![](images/coucou.jpg)

JJ'ai pu discuter de cet essai avec les fab-manageuses qui m'ont conseillée de travailler une forme fermée afin d'obtenir un objet formé de deux courbes simples et pures, à la manière de la LCP de Maarten Van Severen. J'ai donc continué le prototype en ajouter les parties manquantes. Cela a eu pour conséquence de désaxer le centre de gravité de l'objet et donc d'engendrer un renversement de l'objet. J'ai donc décidé de ne pas suivre l'idée de prolongation des éléments durs de l'objet.

![](images/1.jpg)

Ces essais m'ont permis de définir les angles d'inclinaisons des parois des moules formant les blocs en biais servant à la création des courbes de l'objet. 

![](images/inclinaison.jpg)

## Profilés de éléments traversants 

Dans un second temps, j'ai travaillait sur la définition d'un profilé optimale dans un but d'économie de matière et de résistance. Ces profilés ayant pour but de former l'assise et le dossier de l'objet. Une certaine flexion fut également souhaitée afin de conforter l'usage de l'objet. J'ai donc travaillé quatre profilés réalisés à l'aide de moules. Ces essais reproduisent à l'échelle 1/4 la quantité de matière nécessaire et les déformations possibles.

![](images/3.jpg)

Une fois moulés, ces quatre éléments en plastique furent testé en appliquant une force équivalente à 2 kilogrammes. Dans l'Illustration ci-dessous, on peut voir les déformations propres à chaque profilé. J'ai écarté deux profilés de ma sélection (le 3 et le 4) car ceux-ci étaient soirs trop lourd et trop rigide soit simplement trop déformable.  Mon choix s'est tourné vers Les deux premiers essais car ceux-ci lient flexibilité, légèreté et résistance. La réalisation de profilés à l'échelle réelle se fera donc dans un entre-deux de ces morphologies.

![](images/salut.jpg)

En plus d'avoir pu trouver une morphologie intéressante, je me suis également rendu compte que la réalisation de moule en MDF non traité ou renforcé n'est pas favorable à des moulages en grande quantité. En effet, les moules se sont cassé plusieurs fois. Il est donc impératif de trouver une manière de les renforcer afin de faciliter le démoulage et leur pérennité.

## Echelle réelle
### fabrications des éléments
#### Modelisation digitale 

La réalisation des différentes pièces, modules, formant l'assise est fabriquée par moulage. En effet, la matière plastique recyclée est fondue pour ensuite être pressée dans des moules en bois réalisés grace à l'usage d'une fraiseuse numérique. Pour ce faire, Les différentes pièces formant les moules doivent être modélisées puis simulées par l'option d'usinage du programme Autodesk Fusion 360. La simulation de découpe est propre à chaque machine et à chacun post-processeur, cependant, vous trouverez les modèles 3Ds des pièces nécessaires dans le lien Drive suivant : 

https://drive.google.com/drive/folders/1GhzoLXZNGCfqjLnGb65f6_lCYGpEOYCg?usp=sharing

![](images/modèle_3D.jpg)

#### Découpe 

Comme dit plus haut, la découpe des moules est réalisée à l'aide de la CNC. Les pièces nécessaires sont découpées dans deux plaques de MDF. Un de 18 mm et une seconde de 12 mm. Le parcours complet de la fraiseuse prend au total 2h30 pour réaliser toutes les pièces nécessaires. 

![](images/IMG_1738.mov)

#### Assemblage des moules

Une fois toutes les pièces découpées, celles-ci sont poncées puis collées en elle-même à l'aide de colle à bois. Pour assurer un bon maintien des déférentes pièces, des vis sont également ajoutées.

![](images/IMG_2052.JPG)

Après assemblage, les moules sont vernis en 3 couches sur les surfaces restées intactes et en 5 couches sur les surfaces coupées dans la tranche car la matière est beaucoup plus poreuse et absorbante. Il en résulte des moules beaucoup plus solides et moins adhésifs. Le choix de vernir les moules manne de différents échecs précédents. En effet, le MDF a tendance à se fissurer voir s'arracher lorsque l'on démoule les pièces en plastique. De potentiels futurs prototypent de moules devraient faire usage d'une autre matière fonctionnant aussi bien en compression (lorsque l'on presse la matière) qu'en traction (lorsque l'on démoule les pièces).

![](images/IMG_2053.JPG)

#### Moulage du plastique

Le moulage du plastique doit d'abord commencer par récupération de celui-ci. Deux provenances de plastique ont été utilisées pour la fabrication des pièces. Une première source de plastique fut une ferme de la région de Louvain-la-Neuve. Celle-ci amassait du plastique d'emballage de ballot de paille et de sac de fourrage depuis plusieurs années. La seconde source de plastique fut "RECY-K". Il s'agit d'une start-up bruxelloise travaillant sur le recyclage du plastique en le transformant en plaque de construction. Ils ont gentiment accepté de me donner quelques kilos de plastique en copeau. 

![](images/HDPE_recy_K.jpg)

Le plastique récupéré est du HDPE. Ce plastique fond et devient malléable aux alentours de 170°C. On peut donc le faire fondre soi-même à l'aide d'un four ou d'un grill (dans mon cas). 

![](images/fondage.jpg)

Le plastique est roulé en boudin une fois ramolli. Les tous sont ensuite rechauffées quelques minutes. Cette opération est répétée plusieurs fois afin d'obtenir un boudin de la taille du moule.

![](images/boudin.jpg)

Le plastique est ensuite inséré dans le moule et pressé à l'aide de serre-joints ou d'une presse. Un capuchon pour chaque moule a également été découpé. Il permet de resserrer la matière et d'obtenir une pièce plus régulière.

![](images/Capture_d_écran_2021-01-20_à_09.23.39.png)

![](images/moulagesss.jpg)

Après refroidissement des pièces, celles-ci peuvent être facilement démoulées puis nettoyée en enlevant l'excès de matière ayant débordé du moule lors du pressage.

![](images/IMG_2084_copie.jpg)

Voici toutes les pièces réalisées afin de construire une chaise complète.

![](images/IMG_2093.mov)

### Assemblage n°1 : Sangle collée

Le premier essai d'assembler suit le dessin original de l'objet que je voulais créer. C'est-à-dire, aligner les différents éléments en plastique de manière serrée sur deux sangles en les collant. Le tout ayant pour but d'être mit en tension afin de rigidifier la structure comme cela fonctionnait sur les prototypes à l'échelle 1/4. 

![](images/ligne_1.jpg)

J'ai donc collé les différents éléments à l'aide de deux colles censée être ultrarésistante. Dans un premier temps, avec une colle époxy et dans un second temps avec une colle de constructions censées éviter l'utilisation de vis ou de clous. Après utilisation et séchage, je me suis rendu compte que la colle ne suffisait pas à fixer les différents éléments sur les sangles et que donc il faudrait certainement faire nuage de vis afin de faire tenir les profilés.

![](images/collage.jpg)

Une fois l'objet complet, j'ai testé la mise en tension, mais cela n'a pas fonctionné que l'assise et le dossier on fait office de bras de levier sur la courbe liante la base à l'assise. Il était donc impératif de trouver une méthode permettant de mettre l'objet encore plus en tension afin de précontraindre le tout.

![](images/test_total_1.jpg)

### Assemblage 2 : Colle et Vis plus montant arrière

Le deuxième prototype à échelle réelle garde les mêmes principes que les précédents à l'exception du fait que deux montants en bois ont été ajouté à l'arrière de la chaise afin de faciliter la mise en tension et le maintien de l'objet. 

![](images/ligne_2.jpg)

Cette fois-ci, l'objet se maintenait beaucoup mieux, mais une fois que l'on appliquait du poids sur l'assise, la résistance laissait grandement à désirer. L'utilisation de montant ce n'est donc pas suffisant pour la mise en tension de l'objet.

![](images/IMG_2092.JPG)

### Assemblage 3 : Cable gainé et système de poulie

Le troisième test consiste à remplacer les sangles par un câble flottant passant à l'intérieur de chacun des éléments en plastique de sorte qu'une fois mit en tension, ceux-ci puissent glisser le long du câble et se resserrer davantage. Théoriquement, ce système est censé fonctionner, mais cela ne reste que la théorie. 

![](images/cable_1.jpg)

Pour ce faire, chacune des pièces fut percée selon un même axe à l'aide d'une colonne de perçage. Le câble gainé ayant un diamètre allant de 3.5 mm à 5 mm, j'ai pris une mèche de 5 mm de diamètre pour forer les trous nécessaires au passage du câble.

![](images/colonne.jpg)

J'ai ensuite fait glisser le câble dans chacun des éléments afin de pouvoir les mettre en tension par un système de poulies réalisé à l'aide de roulette en acier

![](images/poulie.jpg)

Encore une fois, le résultat n'est pas du tout satisfaisant. La mise en tension du câble reste impossible malgré le système de poulies. Cela est certainement dû au fait que le câble soit gainé de plastique. En effet, le plastique du câble et le plastique de montant étant en contact direct, cela génère trop de frottement et empêche donc la fluidité de mouvement du câble au travers des pièces.

![](images/IMG_2109.jpg)

### Assemblage 4 : cable non-gainé et système de poulie

Le dernier essai consistait à garder le même principe que le précédent. La seule différence était que le câble gainé fut remplacé par un câble non gainé afin de favoriser les mouvements du câble au travers des pièces. Par sécurité, le percement des pièces en plastique fut également agrandi, jusqu'à 6 mm alors que le câble n'en fait que 4. 

![](images/IMG_2110.jpg)

Hélas ce fut encore un échec, la matière reste détendue et non structurante. 

## Un pas en arrière

Après constatation de ses différents échecs, on m'a conseillé de faire un pas en arrière pour rebondir. J'ai donc repensé à certaines choses ayant été mentionné lors de corrections et je suis également retombé sur une photo que j'avais faite lors du premier montage.

![](images/retour.jpg)

Sur cette photo, on peut voir que sans le dossier de la chaise, l'assise tient sans même être mise en tension. Il est donc possible de créer non une chaise mais un tabouret plus simple composé de deux courbes. Après déduction, le problème des essais précédents était la présence d'une contre-courbe au passage de l'assise au dossier.
Après discussion, on m'a conseillé de simplifier le dessin de l'objet que je désirais créer.

![](images/nvx_modele.jpg)

De manière générale, ma recherche s'est plus tournée vers un travail et une étude structurelle 
_________________________________________________________________

Voici maintenant le parcours complet de ma recherche qui m'a ammené à créer le projet de Design expliqué plus haut.

## Maarten Van Severen

![](images/Maarten_Van_Severen.jpg)

### Maarten Van Severen

Maarten Van Severen est un designer Belge, originaire d'Anvers. Issu d'une famille d'artistes, il sera influencé par la recherche de l'abstraction et de la forme la plus pure vers la définition archétype dans son travail. En effet, le père de Maarten Van Severen, Dan Van Severen est un peintre abstrait. Il cherche à dépasser les apparences afin de trouver l'image de l'idée pure. Son fils étudiera à la faculté d'architecture de Gand pour en 1986, se lancer dans la création du meuble en ouvrant son atelier de fabrication. Maarten Van Severen est donc un concepteur fabricant. Il expérimente la matière directement par lui-même afin d'en sentir chaque détail, chaque spécificité.

![](images/atelier_Van_Severen.jpg)

### Projets 

Maarten Van Severen a dans un premier imaginé, pensé et fabriqué des objets uniques dans son atelier. Il a notamment collaboré avec le bureau d'architecture OMA pour lequel il concevra l'intérieur de la maison Lamoine à Bordeaux ainsi que l'intérieur de la maison Dal Ava en bordure de Paris. On voit déjà une recherche de lépuration dans son travail ; Avoir les lignes les plus pures, écarter ce qui est de trop, trouver l'essence même de l'utilisation de l'objet. 

![](images/Villa_OMA.jpg)

Maarten Van Severen est principalement connu pour la conception d'objets simple (mais à la fois complexe) et épuré. on voit dans la majorité de ses oeuvres qu'il exploite un matériau dans sa totalité, afin d'écarter tout surplus. Si l'objet est plus encombrant que l'usage, ça ne va pas...

![](images/travaux_Van_Severen.jpg)

### Industrialisation

En 1994, il reçoit dans son  atelier Rolf Fehlbaum, le PDG de Vitra (société d'édition de meuble et d'objet) avec qui il va s'associer. La production d'objets va alors changer et s'industrialiser. La chaise 02 originellement composée d'une assise et d'un dossier en bois, va être remplacée par du polyuréthane. Les chaises vont également devenir empilables. 

![](images/chair_N2-3.jpg)

Maarten Van Severen va également par la suite s'associer à la société Kartell pour la commercialisation et l'industrialisation de la LC95 qui va devenir la LCP. Le métal est remplacé par l'acrylique teinté. En effet, Kartell est connu pour l'utilisation première et presque exclusive du plastique dans la fabrication d'objet. 

![](images/LC95AMaartenSeveren6_l.png)

## LCP 

![](images/LCP_1.png)

### Contexte de création et influence

La chaise basse LCP (Low Chair Plastic) fut créée en 2002. Il s'agit d'une réinterprétation de la chaise LC95, une première assise basse ayant la même morphologie, conçue en 1993. Il s'est inspirée premièrement des fauteuils bas que l'on retrouvait originellement devant l'âtre des maisons afin d'être à hauteur des sources de chaleur. 
Maarten Van Severen a longuement travaillé avec le bureau d'architecture OMA et le désigner Hans Lensvelt. Il était chargé des intérieurs de différentes villas imaginées par Rem Koolhaas tel que la villa Dal Ava en bordure de Paris ou la maison Lamoine à Bordeaux. De manière générale, nous pouvons déduire que l'architecture émanente du bureau OMA a influencé la création d'objet de Maarten Van Severen. On retrouve un dessin des objets très épuré où tout ce qui n'est pas absolument nécessaire est soustrait. Les objets qui en ressortent son simple mais le fruit d'une réflexion complexe.

![](images/LC95AMaartenSeveren6_l.png)

### Design et Innovation

#### Confort et assise

La LCP et premièrement la LC95 furent conçus dans un but de réconfort. Comme exprimée plusieurs hauts, le dessin de ces objets s'inspire directement des assises placées devant les cheminées. Les nouvelles assises de Maarten Van Severen sont pensée pour amener le sentiment de légèreté et de souplesse. De plus, les parois sont pleines afin d'empêcher les courants d'air de passer. L'acrylique formant l'objet n'est pas un choix annodun non plus. Il s'agit d'un matériau absorbant et rejetant facilement la chaleur. "Lorsque l'on prend place dans cette chaise, ce fauteuil, tout est doux et léger."

#### Structure

La système structurelle de la chaise est innovation en soient les structures minimalistes a permis d'economiser la matière et de la faire travailler en traction, en compression et en flexion. Ces morphologies ramènent la sensation de lévitation déjà présente dans des objets plus anciens tels que les chaises de Marcel Breeur ou Mies Van Der Rohe. Les différents noeuds structurels peuvent, par l'utilisation de plastique, de métal et de résine, être réalisé par pliage. Réduisant donc l'objet à sa forme structurelle la plus pure. Dans le cas de la chaise LCP, une spirale d'acrylique et dans la chaise originale LC95, en aluminium.
Cette structure implique également un facteur de déformation important. En prenant place dans l'un de ces fauteuils, la matière se plie et viens redresser le dossier pour qu'il épouse le dos de l'utilisateur. Ces formes sont en réalité dynamiques. Elles suivent les mouvements du corps lors de l'assise et permette un maintien parfait.

![](images/GIF_LCP.mp4) 

![](images/gif_LC95.mp4)

#### transparence et Couleur

L'utilisation de plastique transparent ou de résine permet à l'objet de s'affiner tout en restant robuste. De plus, la lumière vient s'y refletter et créer un jeu subtil de couleur. L'utilisation de plastique à haute résistance s’immisce dans la fabrication de mobilier depuis la fin des années soixante. 
L’utilisation d'un tel matériau procure aux objets une certaine légèreté, une rapidité de mise en œuvre et dans certains cas, de la transparence. De plus, le plastique permet d’obtenir un rendu propre et lisse. L’utilisation de cette matière a permis de concevoir des objets minimalistes.

![](images/LCP1.jpg)

## Méthodologie du designer

De manière générale, la méthodologie mise en place par Maarten van Severen consiste en la compréhension la plus complète d'un objet et son utilisation dans son contexte pour ensuite en repenser la conception totalement afin d'en extraire la forme la plus pure et la plus juste pour son utilisation. Maximaliser son usage réel et non son aspect superficiel. Cette méthode peut s'appliquer à n'importe quel objet, elle n'est pas exclusive à la réalisation de mobilier. Maarten Van Severen en a également fait l'expérience sur des objets du quotidien tels que des couverts.

[![](images/interview_Maarten_Van_Severen.jpg)](https://www.youtube.com/watch?v=PPE2AC8wzkc)

Quant à la question de la matière, celle-ci dépend du type de production souhaité. Maarten Van Severen n'a pas utilisé le plastique ou d'autre polymères dans son atelier. Ceux-ci sont arrivé dans une seconde phase industrialisée comme expliquée plus haut. Les matériaux qu'il utilise sont souvent le choix d'un type d'assemblage. Les finitions doivent être les plus fines possible afin de ne pas obstruer la nature pure de l'usage de l'objet.

## One-shot's

Je me suis lancé dans différents exercices d'one shots afin de pouvoir pleinement saisir la méthode de conception de Maarten Van Severen. Les différentes réflexions suivantes consistent en le re-dessin d'objet du quotidiens. 

### Bureau

Je me suis exercé à une première one-shot en suivant la même méthodologie utilisée par Maarten Van Severen dans ses travaux. Il est important de préciser que cet essai a été réalisé en un temps limité.

L'objet de référence analysé est mon bureau, constitué de deux tréteaux en bois réglable afin de pouvoir changer le bureau en une table inclinée facilitant le dessin. Ceux-ci disposent également d'un espace de rangement à leur base servant à contreventer la structure. 

Une plaque vitrée fait office du support afin de pouvoir faciliter le travail de décalage en fixant deux lampes sur les tréteaux. Cependant, la feuille de verre et la structure en bois sont dissociées. Lorsque la table est inclinée, deux cornières métalliques viennent retenir les glissements de la table.

Deux lampes réglables sont placées sur le bureau. Celles-ci sont problématiques dans le sens ou elles prennent énormément de place et dans le cas où je souhaite décalquer, elles doivent être déplacée en dessous du bureau et obstruent la place des jambes.

![](images/one_shot_bureau_base.jpg)

Le but de cet one-shot consiste principalement à rendre ce bureau compact tout en préservant sa modularité, ses espaces de rangement et son système luminaire. Cette esquisse consiste également à penser les détails de finissaient et les assemblages de différentes matières.

![](one_shot_bureau.jpg)

La structure a été repensée afin de pouvoir l'épurer au maximum et de pouvoir la moduler sans devoir deviser et régler certaines pièces. Les tréteaux en bois sont donc remplacés par deux profilés en aluminium flexible plié afin de recréer les espaces de rangement. La flexibilité de l'aluminium permet également de régler l'inclinaison de la table suivant un rayon de courbure.

![](images/gif_one_shot.mp4)

Par la suite, j'ai réalisé une maquette du bureau repensé afin de vérifier les déformations de manière plus réaliste. Ce modèle montre que les deux structures ne se déforment pas comme prévu. Normalement seul un des deux montants doit pouvoir s'incliner, se courber. Dans les photos qui suivent, nous voyons clairement que les deux montants des structures se courbent légèrement. L'inclinaison de la table n'est donc pas aussi importante que prévue.

![](images/photo_maquette_table.jpg)

Il est important de préciser que la maquette est réalisée en PLA par impression 3 D, dans la réalité, les structures seraient fabriquée en aluminium ou en acier. Cependant, le dessein peut déjà être repensé afin d'optimiser les déformations souhaitées. J'ai imaginé les deux tréteaux comme un voile simple en métal qui se plie afin de créer les angles souhaités. Mais de cette manière, toute la structure est déformable. Il faut donc rigidifier la structure afin de ne laisser qu'un seul des deux montants flexible. J'ai donc imaginé une manière de rendre ses montants tubulaires en travaillant la structure dans deux plans au lieu d'un seul.

![](images/correction_pied_.jpg)

![](images/Strcures_bis_table.jpg)

![](images/structure_table_revue_maquette.jpg)


Le support de la table est réalisé en un panneau en acrylique afin de permettre à la lumière environnant de se diffuser au travers de la matière. De cette manière, la table devient lumineuse et permet de travailler le décalage sans déplacer de luminaire. La matière est également plus absorbante thermiquement en comparaison avec le verre, ce qui permet d'obtenir un confort de travail supérieur.
Trois encoches sont placées dans l'axe de chaque pied afin de régler l'inclinaison de la structure (voir coupes).

![](images/one_shot_table_rendu.jpg)

### Chaise pliante

Le second exercice de One-shot que je me suis lancé est de repenser la "chaise pliante". Comment redessiner la manière dont l'objet change de volume une fois plié.

Le début de la réflexion commence d'abord par l'épuration des formes afin d'en extraire les lignes les plus pures. De cette manière, la matière s'efface. Ces dessins génériques sont alors l'objectif du projet. "Comment matérialiser ces lignes pures"

![](images/chaise_pliante_base.jpg)

La chaise pliante s'épure de manière à ne plus laisser apparaitre d'élément mécaniques. une fois replier la chaise est réduite à une surface plane, sans macros, sans éléments saillants qui pourraient corrompre le principe de l'objet compact et rangeable.

![](images/one_shot_chaise_pliante_.jpg)

Pour éviter des éléments mécaniques, la matière est travaillée afin de la rendre souple et pliable. Dans cet essai, la chaise est composée d'une seule plaque de bois Multiplex. Une fois celui-ci nervuré, il est possible de le rendre flexible et de le plier.

J'ai donc réalisé quatre prototypes de pliage de multiplex en essayant de garder le moins de matière. Il s'agit de la technique utilisée par Maarten Van Severen lors de la réalisation de la chair 02. Le premier essai fut d'enlever un maximum de matière dans la zone du pli afin de ne laisser qu'une seule couche de bois de la plaque de multiplex. Ce prototype laisse une épaisseur de bois donc les fibres sont perpendiculaires au pli. Il en résulte une flexibilité presque nulle, j'ai donc essayé, pour un second test, de laisser une épaisseur de bois donc les fibres sont parallèles au pli. La pièce est alors très flexible mais une fois passé un certain. rayon de courbure, celle-ci devient trop fragile et se brise.

![](images/test_bois.jpg)

Alors dans un second temps, j'ai opté pour une technique moins économe en matière. La zone de pli n'est donc pas totalement évidée. Quelques rainures viennent assouplir la planche de bois. J'ai donc réalisé des soins perpendiculairement puis horizontalement sur matière. Lorsque les traits sont perpendiculaires à la matière, le bois garde une bonne résistance mais à tendance à s'effriter et à craquer. Alors que lorsque les reignures sont parallèles aux fibres, le pli se fait parfaitement et la pièce de bois garde une résistance suffisante.

Il en résulte donc un objet compact qui se déploie et se referme de manière souple et épurée. 

![](images/chaise_one_shot_rendu.png)

### Lampe

Le troisième exercice d'one shots consiste l'élaboration d'une lampe à la manière de Maarten Van Severen. Ce qui est différent dans c'est one shots, c'est que l'objet de référence est déjà un objet minimaliste et pensé pour être le plus adapté possible tout en cherchant à s'effacer. Mais est-ce que cette lampe est elle vraiment maximaliser dans son usage ?

![](images/one_shot_lampe.jpg)

Après avoir utilisé cette lampe durant plus d'un an, j'ai pris la décision de ne plus l'utiliser pour différentes raisons ; l'angle de courbure n'est pas adapté, l'intensité lumineuse n'est pas assez importante et les dimensions globales de l'objet ne sont pas suffisantes pour en faire un objet pratique et vraiment maximisé.

![](images/One_shot_chaise_bis.jpg)

Je suis donc parti du même schéma générique de la lampe de référence. Une ligne pure qui se courbe.Les proportions de l'objet sont également revues afin que la lampe soit adaptée à différents usages en fonction de la position que l'utilisateur adopte (debout, assis, couché).

La lampe est fabriqué à partir d'un profilé tubulaire en aluminium. Cela permet de dissimuler les éléments électiques et d'épurer la structure. Il est possible de courber ce profilé en créant des entailles tout le long du profilé. 

![](images/test_pli_tube.jpg)

![](images/IMG_9172.mov)



Il s'agit donc d'un objet réglable, qui s'adapte à l'usage que vous souhaitez tout en dissimulant les éléments qui le rendent modifiable. Cette lampe est pensée comme un objet fonctionnel plus que comme un objet plaisant à regarder.

![](images/rendu_lampe_one_shot.jpg)


# Remise en question pour la suite du projet

Après la discussion, j'ai besoin de remettre en question mon approche du projet. "Imaginer des one-shot's , c'est bien mais ce n'est pas concret". Il faut donc que je rentre dans un projet concret et travailler le prototypage à l'aide des outils du Fablab. 

D'un autre côté, j'ai constamment gardé en tête la méthode de Maarten Van Severen en oubliant les objets même. Il serait donc intéressant que je retourne au concret de son travail, à savoir l'objet de référence, la chaise LCP et la chaise LC95. En travaillant sur la réduction de l'objet, j'en ai oublié sa méthode de fabrication, qui n'est absolument pas négligeable. Je dois donc également penser au fait que les objets sont travaillés en une seule matière qui se courbe selon un seul plan. De cette manière, les lignes s'affinent et plus l'objet est fin, plus il a une "puissance" importante dans la vision minimaliste.

La suite du projet va donc partir du matériau pour arriver ensuite à un projet. Je compte donc travailler sur une seule et même feuille de matière et voir comment celle-ci peut se déformer selon la réduction des lignes d'un objet vers une forme de l'ordre de l'archétype.

## précision sur le projet 

a recherche du pliage de bois et de matière parfaite et lisse m'a paru moins intéressante qu'une recherche nouvelle sur une matière moins connue. En effet, j'ai déjà pu voir de nombreux exemples montrant comment le bois peut se plier lorsqu'il est chauffé à la vapeur ou lorsqu'il est entaillé afin de le rendre plus flexible. 

J'ai donc voulu me pencher, dans la suite de mon travail de l'année passée, sur une recherche structurelle du plastique. Comment rendre des feuilles ne plastiques plus résistance afin de pouvoir en fair un objet rigide tout en gardant une flexibilité afin qu'il puisse être modulable.

![](images/12989_une.jpg)

## Minilalisme et ses codes

*"En général, les caractéristiques du minimalisme incluent des formes géométriques, souvent cubiques, purgées de métaphore, l'égalité des parties, la répétition, l'usage de surfaces neutres et de matériaux industriels."*

*"Reposant sur le concept du "Less is more", cher à Mies van der Rohe, le design minimal se concentre sur l'essentiel. Il mise sur la sobriété et les lignes épurées. Cette sobriété passe par les couleurs. En effet, le style minimaliste est le strict opposé du style pop et déteste les couleurs criardes."*

*"Le design vise à créer des systèmes interactifs si simples qu'ils ne sont plus reconnaissables en tant que systèmes, mais disparaissent en arrière-plan, améliorant tranquillement nos capacités."*

*"Les utilisations possibles de la notion de minimalisme dans le domaine de la conception d'interactions homme-machine sont examinées avec une attention à la fois théorique et empirique."*

*"Il existe un argument continu et controversé sur la relation et la hiérarchisation de l'utilité et de la beauté dans la conception et sur l'influence de l'esthétique sur la convivialité ou l'utilité des artefacts (...) La beauté de la machine est créée par la combinaison de la puissance et de la simplicité. La beauté est donc plus qu'une peau profonde, plus qu'un sentiment à mesurer; c'est une explication de la qualité intrinsèque de «l’outil» qu’un «bon» design émet"*

Suite à la consultation et à la lecture de différents textes, avis, travaux sur le sujet du minimalisme, j'ai pu tendre à une vision du minimalisme dans sa globalité. Différents codes de pensée et de conception sont apparus évidents.

![](images/objets_minimalistes_2.jpg)

  * La puissance de l'objet apparaît lorsque la forme d'usage est la plus simple et que les médiums de constructions s'effacent.
  * Les formes générées ne sont pas sujettes aux normes que nous pouvons nous faire d'un objet. Les formes suivent l'usage de manière abstraite.
  * Les déférentes parties d'un objet sont mise à égalité afin que l'objet ne soit pas sujet à une forme de domination des parties qui le composent. On voit une répétition qui se forme.
  * Les systèmes structurels s'effacent afin de laisser place à un objet autonome.
  * Seule la recherche empirique de l'usage peut mener à une forme archétypale de l'objet.
  * La chromatique possède une place importante dans la conception. Elle fait partie intégrante de la simplicité et de la mise à égalité des parties de l'objet.

Il est important de préciser que ces codes interprétés viennent de la vision que les concepteurs se faisaient du minimalisme au début des années 2000. 


## Esquisse 

![](images/esquisse_finale.jpg)

## Poles du Design

**FORME / MATIERE / ASSEMBLAGE**

La conception et la recherche de l'objet que je cherche à travailler suivent trois pôles génériques qui viennent qualifier le design de l'objet final. Ces trois pôles touches ; la forme, la matière, L'assemblage. 

### Forme 

La forme doit correspondre à une abstraction que nous pouvons nous faire de l'usage d'une assise. Mais cette forme doit est double : "L'objet rangé et l'objet utilisé". Il faut donc penser à la fois l'archétype de l'usage et l'archétype du nan usage.

### Matière 

La matière première utilisée est le plastique de récupération. Celui-ci est traité afin de recréer une matière brute qui va servir de surface de travail pour le développement de l'objet. Il s'agit ici de recréer une matière brute nouvelle propre au contraite actuel de notre génération. 

Cette matière possède également des caractéristiques propres. Celle qui va le plus nous intéresser dans notre développement est sa résistance à la déformation. Il est donc important de prendre en compte le ce facteur.

### Assemblage 

Le système d'assembler tend ici à suivre le principe de la répétition des éléments. La dissociation des éléments permet passait d'un objet structurel à un objet maniable pouvant être rangé. Il y a donc besoin de chercher la manière dont le prototype peut-être mit en tension à certains moments.

# Recherche sur la matière

J'ai d'abord voulu aborder la matière plastique sans avoir une idée précise de l'objet final. Comment rendre des éléments de plastiques résistant tout en restant flexible. Dans un premier temps, j'ai commencé à travailler sur des éléments en plastiques recyclables, des emballages plastiques et des sacs de courses.

## Profilés (blocs plastique)

### Méthodologie

La méthode de production des différents profilés de plastique consiste en la création d'un moule dans lequel le plastique sera pressé. Il s'agit ici de recréer le système de plastique injecté avec les outils digitals du Fablab.

![](images/methodologie_plastique.jpg)

### Moules 

NB: comme il s'agit d'un essai de matière, les moules ont été crée aléatoirement. Les inclinaisons ne pas correspondre à la création d'une courbe précisément définie.

Les moules sont fabriqués par l'utilisation de la fraiseuse numérique (CNC). Ceux-ci sont découpés dans une planche de bois multiplex. Ne pouvant pas découper une moule dans un bloc massif, il est donc obligatoire de décomposer les moules en différentes sections afin de pouvoir les assembler pour obtenir un négatif de l'objet du gabarit souhaité.

![](images/moule_testt.jpg)

### Plastique

Le plastique est ensuite coulé dans les moules fabriqués. Pour ce faire, le plastique est d'abord coupé en morceaux plus petits afin d'accélérer leur liquéfaction lors de la chauffe. Celle-ci peut se faire selon deux méthodes ; par l'utilisation d'un four traditionnel chauffé à 170 degrés Celsius ou par l'utilisation d'un grill permettant un contact direct entre la source de chaleur et le plastique. Cette seconde méthode permet un gain de temps non négligeable.

![](images/melt_plastique.jpg)

Le plastique est donc coulé et pressé dans les moules à l'aide d'une presse, d'un étau ou de sert joint.

![](images/blocs_plastique_1.jpg)

##  Membrane de mise en tension (multiplex de plastique)

### Plastique - Plastique
Afin de rigidifier le plastique, j'ai d'abord essayé de dans un multiplex en multipliant les couches et en les liants. Pour ce faire, les couches sont superposées et chauffées à l'aide d'un fer à repasser. Les différentes couches se lient et forment ainsi une toile plastifiée plus résistante tout en restant flexible. J'ai également réalisé différents tests avec un nombre de couches différentes afin de voir quel multiplex est le plus résistant et le plus flexible.

![](images/test_multi.jpg)

J'ai également réalisé un test avec un plastique différent (thermodurcissable). La liaison des couches a malheureusement mal fonctionné car ce type de plastique a tendance à se rétracter sans se lier.

Le prototype ayant 3 couches de plastiques est celui qui me paraissait le plus convainquant car il est assez résistant tout en restant relativement flexible alors que les tests ayant un nombre de couche supérieure ont tendance à être trop rigide.

### Plastique - Tissu

Dans un second temps, j'ai voulu essayer d'intégrer une membrane de tissu dans le multiplex de plastique. Pour ce faire, une couche de tissu avec une maille relativement large est placée entre deux couches de plastique avant d'être repassé au fer. J'ai réalisé deux tests, l'un des deux, suis composé de deux feuilles de tissu et de deux feuilles de plastique alors que le deuxième, est composé uniquement d'une seule feuille de tissu entre deux feuilles de plastique. Ce second test est beaucoup plus résistant car le tissu est parfaitement imprégné de plastique alors que le premier a tendance à se décoller car le plastique n'a pas pu imprégner les deux feuilles de tissu.

![](images/test_tissu.jpg)

# Structure 

## Mise en tension 

Le système structurel exploité ici est la mise en tension de la pluralité des éléments. Cette mise en tension est alternative. Elle permet de rigidifier l'objet et son assise de la même manière qu'une planche de bois entaillé se rigidifier une fois pliée. Pour créer une mise en tension, il faut apporter un second élément à l'objet. La chaise est donc composée d'une matière qui se déplie tout le long de l'objet et d'un second élément qui vient tendre cette matière.

## comparatif avec la LCP / LC95

Dans son travail sur la chaise LCP, Maarten Van Severen travaille également son objet en deux parties ; une matière unique qui se déroule pour créer la morphologie de l'objet et une deuxième partie qui rigidifie l'objet. Cette deuxième partie est composée de quatre plots en métal posé à la base qui vient relier les deux extremes de l'objet. C'est le seul point fix de la structure qu'il crée. De cette manière, l'objet garde une certaine flexion tout en restant stable.

![](images/structure_LCP.jpg)

### Transposition

Le système structurel exploité dans l'objet travaillé reste parallèle à l'objet pensé par Maarten Van Severen. L'objet garde une certaine flexion, un coefficient de déformation tout en restant stable. La feuille d'acrylique est ici remplacée par les différents éléments en plastique, et les quatre plots en métal sont ici travaillés sous forme d'une sangle qui vient tendre la matière.

![](images/structure_mon_projet.jpg)

# Prototype 1 

Le premier prototype testé consiste à vérifier si la structure souhaitée fonctionne correctement. Pour ce faire, une maquette à l'échelle 1/4 fut réalisée.

## Dessin 

Dans un premier temps, il est important définir l'inclinaison de chaque bloc composant la chaise. Je suis parti du profil d'une assise existante que j'ai ensuite décomposée en différents blocs (32).  On retrouve 3 types de blocs dans la composition : un profil en trapèze régulier, un deuxième profil en trapèze non symétrique et un troisième profil en parallélépipède.

![](images/Capture_d_écran_2020-12-15_à_17.33.05.png)

## Moules 

Trois moules du négatif des trois-pièces sont ensuite modélisés à l'aide de fusion 360 pour ensuite être usiné par la fraiseuse numérique. Ces moules sont découpés dans une plaque de multiplex de 18 mm d'épaisseur. Il s'agit ici d'un modèle 3D paramétrique afin de pouvoir travailler à n'importe quelle échelle souhaitée.

![](images/moule_1_4.jpg)

## Réalisation des blocs

La réalisation des blocs est faite par le moulage de plastique fondu. Ce plastique est ensuite pressé dans les moules à l'aide d'une presse fabriquée à partir d'un cric de voiture. Il faut repeter l'opération trois fois pour le moule trapezoidal asymétrique, 18 fois pour le moule parallélépipédique et 11 fois pour le moule trapezoidal symétrique.

![](images/moulage_nettoyage.jpg)

## Assemblage

L'assemblage des différents éléments en plastique sur les deux sangles est réalisé par l'utilisation de colle. chaque élément est collé à la sangle de manière très serrée. il ne doit pas y avoir de vide entre chaque élément sinon l'objet en tension sera sujet à des déformations trop importantes.

Une fois l'objet mit en tension, on obtient une assise relativement résistante qu'il faudrait travaillait à l'échelle réelle pour vérifier la tenue.

![](images/final_test_1.jpg)

## Conclusion

Ce premier prototype complet permet de voir que le système structurel travaillé de la mise en tension fonctionne. Cependant, on peut voir que l'objet se déforme légèrement lorsque l'on applique une charge dessus. C'est une caractéristique recherchée dans un but de confort mais si la déformation est trop importante, l'assise ne sera pas optimale. Il y a donc un travail à faire sur la gestion de cette déformation par la précontrainte. 

De plus, il serait intéressant de voir comment intégrer les sangles directement dans plastique afin de laisser une surface parfaitement régulière sur la surface de l'assise.

# Prototype 2

Ce second prototype complet prend en compte la question de la déformation et de la précontrainte. La morphologie globale de l'objet n'a cependant pas changé.

## Précontrainte

La précontrainte de l'objet est travaillée par la modification du profilé des blocs composant l'assise. Pour se faire les angles sont grossies afin d'obtenir une pièce plus large prenant en compte le facteur de tassement et de déformation (élasticité du plastique).

![](images/profil_précontrain.png)


## Nouveaux moules

Les moules sont réalisés de la même manière que les précédents. Cependant, l'inclinaison des parois étant beaucoup plus faible, il est important d'affiner l'usinage du bois afin de ne pas avoir des montants obliques en escalier mais bien en pente douce et propre.

![](images/moule_2.jpg)

## Assemblage 

L'assemblage suit également la même méthode que le premier prototype complet. Les éléments en plastique sont collés sur les sangles.

![](images/proto_2.jpg)

## Conclusion

Ce second essai permet de voir que la question de la précontrainte est importante. Cependant, celle-ci ne doit pas s'appliquer à toutes les pièces de l'objet comme je l'ai fait. L'assise doit être précontrainte mais pas le dossier sinon la forme générale de la pièce ne respectera pas le confort souhaité pour la chaise.

![](images/IMG_1766_copie.jpg)

# Suite

Différents éléments doivent être améliorés afin d'affiner les détails de l'objet pour tendre à une morphologie archétypale. "Plus les détails de construction sont fins, plus la puissance de l'objet sera importante".

## Liaison des éléments de tension aux blocs

Le travail de liaison des éléments en plastique à la sangle est important. l'élément structurel est encore trop visible et écarte donc l'objet de la finalité minimaliste cherchée. 

![](images/IMG_1767_copie.jpg)

J'ai déjà pu réaliser deux tests d'intégration de la sangle dans le plastique. Le premier en pesant les éléments en plastique pour faire passer la sangle à l'intrus de la masse. Et le second en coulant/pressant le plastique sur la sangle alors que celui-ci est encore chaud. Le moule doit donc prendre en compte la position de la sangle.Ces deux tests ne sont pas du tout concluant à mes yeux, il va donc falloir revenir sur cette question.

![](images/Sans_titre-8.jpg)

## Objet paramétrique / Précontrainte 

J'aimerais également retravailler la question de la précontrainte de manière plus précise afin de pouvoir créer les moules les plus adaptés en fonction du poids et de la taille de chaque utilisateur. Cela pourrait mener à la création d'un objet paramétrique que n'importe qui pourrait reproduire dans un Fablab en encodant ses propres mensurations.

## Echelle 1/1

J'ai également pu réaliser des moules à l'échelle réelle pour pouvoir tester l'objet à l'échelle réelle.

![](images/IMG_1772_copie.jpg)
