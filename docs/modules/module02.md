![](../images/ORIGIN_.jpg)

## La Shaper c'est quoi ?

La shaper Origin est une défonceuse numérique assistée. En d'autre mots, il s'agit d'une machine qui est a mis chemin entre la CNC et la défonceuse classique. La machine est déplacée par l'utilisateur sur le plan de travail, mais c'est elle qui rectifie le suivit d'une découpe afin d'obtenir une gravure ou une découpe nette et précise. D'une certaine manière, la machine est assistée par l'utilisateur et l'utilisateur est assisté par la machine.

## Pourquoi la Shaper ?

On peut se demander pourquoi la shaper a été inventée... Pourquoi ne pas continuer à travailler avec une CNC alors que celles-ci sont très précises, et plus rapide ? 

La shaper est une machine qui trouve son sens dans le fait que la surface de travail peut être infinie. Comme la machine n'est pas déplacée selon deux axes fixes et limité dans l'espace, les découpes peuvent se déployer à l'infini. Bien sûr comme il s'agit d'une machine dirigée à la main, nous sommes limités à la distance maximum de nos bras.

La machine est également intéressante pour sa facilité d'utilisation. Lors de l'emploi d'une CNC classique, il est impératif de tout prévoir et tout coder afin que la machine puisse travailler par elle-même lors de la découpe. Cette planification de code cause très souvent des intégrations et des incompréhensions. En revanche, l'utilisation de la shaper Origin est beaucoup plus simple. La machine fonctionne selon un simple dessin vectorisé. Tous les réglages de découpe et de gravure se font directement lors de la découpe. Il est alors plus facile d'intervenir sur la découpe de la pièce sans devoir essayer de se l'imaginer lors de la planification de fichier de découpe classique pour la CNC.

Par interprétation, je peux également imaginer que l'utilisation d'une shaper serait beaucoup plus accessible que l'utilisation d'une CNC. Il s'agit d'une machine moins couteuse, moins encombrante et comme expliqué plus haut, plus facile d'usage.

## Utilisation 

J'ai expérimenté l'usage de la machine afin de pouvoir guider et illustrer l'utilisation de la shaper Origin

### Dessin et fichier préalable 

L'utilisation de la shaper Origin pour une découpe ou une gravure nécessite la création d'un dessin vectorisé (SVG)

J'ai d'abord réalisé un dessin à la main que je souhaiterais graver dans une plaquette de bois Multiplex à l'aide de la Shape.Ce premier dessin est relativement minimalist. Il est donc facilement vectorisable à l'aide d'un programme comme Adobe Illustrator ou Inkscape. Une fois ouvert sur l'un de ses programmes, j'ai décidé de garder uniquement les contours du dessin qui serviront à guider la fraise de la machine.

![](../images/vectorisation.jpg)

Une fois le fichier vectorisé enregistré en SVG, je l'ai placé sur une clef USB afin de pouvoir l'insérer directement sur la machine.

### Instalation du support de découpe

Le set-up de découpe ou de gravure est relativement semblable à l'installation nécessaire pour l'utilisation de la CNC. La matière sur laquelle on désire faire les découpes est accrochée de manière très stable à l'aide de papier collant double face ou de vissés sur une plaque de bois faisant office de martyr. De cette manière, si la découpe traverse la matière à travailler, la fraise va continuer ça découpe du martyr.

![](../images/martyr.jpg)
Une fois que l'installation du support est faite, j'ai déterminé les points de repère de la machine et donc la zone de découpe. Pour ce faire, il faut disposer des bandes collantes de repère conçu pour cette machine. Les dominos présents sur les bandes adhésives sont représ par les capteurs présents sur la machine pour ensuite définir la zone de travail. On peut donc travailler sur tout type de support grace à cette technique de repérage par le shaper-tape.

![](../images/tape.jpg)

### Scan de l'espace de travail

Une fois la machine branchée, l'écran d'accueil proposent différents onglets à régler. La première chose à faire et de scanner l'espace de travail où sont collées les bandes de Shaper-tape en cliquant d'abord à droite de l'écran sur "Scan" puis à gauche sur "nouveau Scan". Il faut ensuite déplacer la machine à la main sur le support afin que les capteurs puissent trouver les bandes de tape et donc se repérer dans l'espace du travail.

![](../images/scan.jpg)

Une fois le repérage terminé, l'espace de travail s'enregistre et est gardé en mémoire. On peut donc le récupérer à tout moment en cas d'une nouvelle déoupe sur le même support.

![](../images/IMG_1585.jpg)

La machine sait donc ou se trouve les limites de l'espace de travail.

### Insertion du tracé vectoriel

Le fichier vectoriel est donc placé sur une clef USB que l'on insère directement sur la machine. Sur l'écran principal, il faut se rendre dans l'onglet "Dessiner puis d'en importer afin d'accéder aux documents de la clef USB. On donc sélectionner notre fichier, pour le disposer sur l'espace de travail en faisant glisser la machine sur le support. Celui-ci est visible grace aux caméras servant aux repaires de la shaper. Avant de positionner notre dessin, nous pouvons également en changer l'échelle et la rotation. En cliquant sur le bouton verre sur la mannette de droite, vous validez l'action que vos êtres en train de faire. 

![](../images/placement.jpg)

### Réglage de découpe

Une fois le document positionné dans l'espace du travail, chaque ligne de découpe s'affiche. On peut donc régler chaque ligne de découpe de manière indépendante. On peut donc régler la position de la fraise par rapport à la ligne guide, la profondeur de coupe et la vitesse de découpe. On peut également changer de fraise entre chacune des lignes.

![](../images/reglage.jpg)

### Découpe

Une fois les réglages définissent, la découpe peut s'effectuent en appuyant sur le bouton vert et en déplaçant la machine en la faisant glisser sur le support. Si une seule profondeur de découpe ne suffit (puisqu'il ne faut pas dépasser la moitié du diamètre de la fraise), on peut fair une deuxième passe en règle à nouveau la profondeur de coupe.

![](../images/test.jpg)

### Notes

- Il est important de brancher la machine à un aspirateur pour garder une surface de déplacement propre.
- La vitesse de rotation dépend du matériau sur lequel on opère. Il est donc parfois nécessaire de faire des tests avant de commencer notre découpe.
- À chaque changement de fraise, il est important de refaire une "mise au point" de l'axe Z (vertical) afin que la machine sache exactement ou commencer ça découpe dans la hauteur.
